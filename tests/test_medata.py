import yaml, os
from glob import glob
from pathlib import Path

def get_all_params(path_to_CONFIGFLAGS):
    """
    Read the LHAPDF CONFIGFLAGS and parse the entries there
    """
    with open(path_to_CONFIGFLAGS, "r") as f:
        lines = f.readlines()

    parameter = {}
    for i, line in enumerate(lines[:-1]):
        if(not lines[i+1].startswith("----")): continue
        key = line.replace("\n","")

        # skip initial two keys:
        if(key == "Verbosity" or key == "Pythia6LambdaV5Compat"): continue

        # also skip memtype key, since this is almost always at member level
        if(key == "MemType"): continue

        # check all nextlines
        mandatory = lines[i+2].startswith("MANDATORY")
        optional  = lines[i+2].startswith("OPTIONAL")

        # check if this is only mandatory under certain conditions
        # in which case we consider this non-mandatory here
        mandatory_if = lines[i+2].startswith("MANDATORY if")
        if(mandatory_if):
            mandatory = False
            optional  = True

        if(not ( mandatory or optional)):
            # should be one of the two, for consistency check if one of them is satisfied
            print(f"ERROR: Parameter {key} in CONFIGFLAGS is neither optional nor mandatory")

        # some lines contain multiple keys
        if("," in key):
            keys = key.split(",")
            for k in keys:
                _k = k.replace(" ","")
                parameter[_k] = {"mandatory" : mandatory}
            continue
        parameter[key] = {"mandatory" : mandatory}

    return parameter

def check(setname, data, key, fallback=None):
    if(key in data):
        return True
    # check if fallback is present
    if(fallback):
        if(fallback in data):
            return True

    # key not present
    print(f"Set {setname} is missing info entry: {key}")

    # check if that is maybe just a spelling error
    # TODO: probably shouldn't always convert all of them
    p_low = {a.lower():a for a in data}
    if(key.lower() in p_low):
        print(f"ERROR in {setname} Check case sensitivity, given {key}, found {p_low[key.lower()]}")

    return False

def check_case_sensitive(key, dct):
    if(key in dct):
        return True
    else:
        return False

def check_case_insensitive(key, dct):
    dct_lower = {a.lower() : a for a in dct}
    if(key.lower() in dct_lower):
        return True
    else:
        return False

def test_basic_set_properties(path_to_set):
    """
    Performs some naive tests on the given PDF set
    Input should be the path to a PDF dir
    """
    dir_ok = True
    infos = glob(f"{path_to_set}/*.info")
    if(len(infos) > 1):
        print(f"ERROR in {path_to_set}: Set contains multiple .info files")
        dir_ok = False

    # match all files and check if their endings are plausible
    all_files = glob(f"{path_to_set}/*")
    for f in all_files:
        if(f.endswith(".dat")): continue
        if(f.endswith(".info")): continue
        print(f"ERROR in {path_to_set}: Set contains unexpected file {f}")
        dir_ok = False

    # match all files and check if they begin with a sensible string
    setname = Path(path_to_set+"/").parts[-1]
    for f in all_files:
        if(f.split("/")[-1].startswith(f"{setname}")): continue
        print(f"ERROR in {path_to_set}: Set contains unexpected file {f}")
        dir_ok = False

    # explicitly check for hidden files
    dot_files = glob(f"{path_to_set}/.*")
    if dot_files != []:
        print(f"ERROR in {path_to_set}: Set contains unexpected dot files: {dot_files}")
        dir_ok = False

    # TODO: should check if everything is named correctly
    # i.e. are they numbered correctly etc
    return infos[0], all_files, dir_ok

def test_metadata(setname, info, all_parameter, verbose = True):

    ok = True
    with open(info,"r") as f:
        data = yaml.safe_load(f)

    # initial pass, is every parameter valid?
    for key in data:
        if(check_case_sensitive(key, all_parameter)): continue
        if(check_case_insensitive(key, all_parameter)):
            print(f"ERROR in {setname}: Check case-sensitivity of Parameter {key}")
            ok = False
            continue
        ok = False
        print(f"ERROR in {setname}: Metadata contains key: {key}, that does not correspond to known entry")

    # second pass, are all mandatory parameters present?
    for key, value in all_parameter.items():
        if(not value["mandatory"]): continue
        if(check_case_sensitive(key, data)): continue
        ok = False
        print(f"ERROR in {setname}: Missing mandatory parameter: {key}")

    # some conditional parameters
    if("AlphaS_Type" in data):
        # this is in mandatory parameter and should be there, if not we hopefully
        # complained earlier
        if(data["AlphaS_Type"] == "ipol"):
            ipol_mandatory = ["AlphaS_Qs","AlphaS_Vals"]
            for key in ipol_mandatory:
                if(check_case_sensitive(key, data)): continue
                ok = False
                print(f"ERROR in {setname}: Missing mandatory parameter: {key}")


        if(data["AlphaS_Type"] == "analytic"):
            # at least one of AlphaS_Lambda3, AlphaS_Lambda4, AlphaS_Lambda5
            # has to be present. If multiple ones are given, they should be
            # in a continuous order
            if("AlphaS_Lambda3" not in data and
               "AlphaS_Lambda4" not in data and
               "AlphaS_Lambda5" not in data):
                ok = False
                print(f"ERROR in  {setname}: Set is missing info entry: AlphaS_Lambda*")

            if("AlphaS_Lambda3" not in data and
               "AlphaS_Lambda4" in data and
               "AlphaS_Lambda5" not in data):
                ok = False
                print(f"ERROR in {setname}: Set has discontinuous set of Alphas"
                      "Lambdas in info file")
    return ok, data

def test_consistency(setname,files, metadata):
    """
    Perform some consistency tests regarding metadata and the
    remaining files.
    """
    set_ok = True

    # count data files
    n_data_files = 0
    for f in files:
        if(f.endswith(".dat")): n_data_files += 1

    if("NumMembers" in metadata):
        if(n_data_files != metadata["NumMembers"]):
            print(f"ERROR in {setname}: Number of datafiles does not coincide with metadata entry 'NumMembers'")
            set_ok = False
    else:
        # NumMembers is mandatory
        # if this is missing, we hopefully complained earlier
        # still return false
        set_ok = False

    return set_ok

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--set', '-s',type=str, required=True, nargs='+',
                        help = "PDF dir to be checked")
    parser.add_argument('--flags', '-c', type=str, required=True,
                        help = "Path to lhapdf/CONFIGFLAGS (including the CONFIGFLAGS bit)")
    args = parser.parse_args()

    path_to_CONFIGFLAGS = args.flags
    all_params = get_all_params(path_to_CONFIGFLAGS)
    everything_ok = True
    for s in args.set:
        info, all_files, dir_ok = test_basic_set_properties(s)
        if(not dir_ok):
            print(f"Error: Set {s} failed tests")
            print(f" --> Resolve issues and try again")
            everything_ok = False
            continue

        metadata_ok, metadata = test_metadata(s, info, all_params, True)
        consistency_ok = test_consistency(s, all_files, metadata)
        if(not (metadata_ok and consistency_ok)):
            everything_ok = False

    if everything_ok:
        print("Everything looking good!")
    else:
        print("Errors found. Check logs for more details")
    return everything_ok


import sys
if __name__ == "__main__":
    ok = main()
    if(ok):
        sys.exit(0)
    else:
        sys.exit(1)
